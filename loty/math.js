"use strict";
exports.__esModule = true;
exports.fib = function (i) {
    if (i < 0)
        return -1;
    if (i < 2)
        return i;
    else
        return exports.fib(i - 1) + exports.fib(i - 2);
};
