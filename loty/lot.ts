let pasazerowie = document.getElementsByClassName("pasazer") as HTMLCollectionOf<HTMLLIElement>;
let pasazerowie_arr = [].slice.call(pasazerowie);

let max = null as HTMLLIElement;

for (let pasazer of pasazerowie_arr) {
    if(max == null)
        max = pasazer;
    if(pasazer.dataset["identyfikatorPasazera"] > max.dataset["identyfikatorPasazera"])
        max = pasazer;
}

console.log("Największe id pasażera ma: ", max.textContent);

