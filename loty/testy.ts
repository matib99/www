import { fib } from "./math";
import { expect } from "chai";
import "mocha";
describe("Fibonacci", () => {
    it("should equal 0 for call with 0", () => {
        expect(fib(0)).to.equal(0);
    });
    it("should equal 55 for call with 10", () => {
        expect(fib(10)).to.equal(55);
    });
    it("should equal 89 for call with 11", () => {
        expect(fib(11)).to.equal(89);
    });
});

