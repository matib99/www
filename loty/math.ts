export const fib = (i) => {
    if(i < 0)
        return -1;
    if(i < 2)
        return i;
    else
        return fib(i-1) + fib(i-2);
};