
document.addEventListener('DOMContentLoaded', (e) => {
    let nameInput = document.querySelector('input[name=imie]') as HTMLInputElement;
    let surnameInput = document.querySelector('input[name=nazwisko]') as HTMLInputElement;

    nameInput.value = 'Jan';
    surnameInput.value = 'Kowalski';
})