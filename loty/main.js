"use strict";
exports.__esModule = true;
var math_1 = require("./math");
window.addEventListener('DOMContentLoaded', function (e) {
    var elInput = document.querySelector('input[name=imie]');
    console.log(elInput.value);
    var elP = document.querySelector('p');
    elP.textContent = 'tekst zmodyfikowany, zmodyfikowany tekst.';
    var nowyDiv = document.createElement('div');
    nowyDiv.innerHTML = '<p><b>NOWY DIV - najnowszy commit w typescripcie:</b></p>';
    var staryDiv = document.querySelector('div[class=container]');
    staryDiv.appendChild(nowyDiv);
    teczoweKolory(nowyDiv);
    tsGitHubInfoPromise.then(function (res) {
        //console.log("WYNIKI:\n", res[0]);
        var avatar = document.createElement('img');
        var author = document.createElement('p');
        var authorDiv = document.createElement('div');
        avatar.src = res[0].author.avatar_url;
        avatar.width = 200;
        avatar.height = 200;
        author.textContent = res[0].author.login;
        authorDiv.appendChild(author);
        authorDiv.appendChild(avatar);
        nowyDiv.appendChild(authorDiv);
    });
    var grid = document.querySelector('div[class=container]');
    grid.addEventListener('click', makeRed, false);
    var form = document.querySelector('form[class=reservationForm]');
    form.addEventListener('change', checkInput);
    var submitButton = document.querySelector('input[type=submit]');
    submitButton.disabled = true;
    form.addEventListener('submit', function (e) {
        var nameInput = document.querySelector('input[name=imie]');
        var surnameInput = document.querySelector('input[name=nazwisko]');
        var selectFrom = document.querySelector('select[name=od]');
        var selectTo = document.querySelector('select[name=do]');
        window.alert("\n        dane rezerwacji: \n\n        Imi\u0119: " + nameInput.value + " \n\n        Nazwisko: " + surnameInput.value + "\n\n        Od: " + selectFrom.value + "\n\n        Do: " + selectTo.value);
    });
}, false);
var numerKlikniecia = 0;
setTimeout(function () {
    console.log('Minęły już 2 sekundy...');
}, 2000);
var waitFor = function (time) {
    var promise = new Promise(function (resolve, reject) {
        setTimeout(function () { return resolve("ok"); }, time);
    });
    return promise;
};
var tecza = [
    'red',
    'orange',
    'yellow',
    'green',
    'blue',
    'indigo',
    'purple'
];
var kolorowaPetla = function (el, kolorowaTablica, i, max) {
    waitFor(1000).then(function () {
        //console.log(kolorowaTablica[i]);
        el.style.backgroundColor = kolorowaTablica[i];
        i = (i + 1) % kolorowaTablica.length;
        if (max > 0)
            max--;
        else if (max === 0)
            return;
        return kolorowaPetla(el, kolorowaTablica, i, max);
    });
};
function teczoweKolory(el) {
    kolorowaPetla(el, tecza, 0, -1);
}
var tsGitHubInfoPromise = new Promise(function (resolve, reject) {
    fetch("https://api.github.com/repos/Microsoft/TypeScript/commits")
        .then(function (res) { return res.json(); })
        .then(function (data) { return resolve(data); });
});
var makeRed = function (e) {
    var delays = document.querySelector('table[class=delaysTable]');
    var form = document.querySelector('form[class=reservationForm]');
    if (!delays.contains(e.target) && !form.contains(e.target))
        return;
    if (e.target.style.backgroundColor === 'palevioletred')
        e.target.style.backgroundColor = 'darkseagreen';
    else
        e.target.style.backgroundColor = 'palevioletred';
    console.log("FIB: ", math_1.fib(numerKlikniecia));
    numerKlikniecia++;
};
var checkInput = function (e) {
    var nameInput = document.querySelector('input[name=imie]');
    var surnameInput = document.querySelector('input[name=nazwisko]');
    var selectFrom = document.querySelector('select[name=od]');
    var selectTo = document.querySelector('select[name=do]');
    var submitButton = document.querySelector('input[type=submit]');
    if (nameInput.value.trim() !== ''
        && surnameInput.value.trim() !== ''
        && selectFrom.value !== null
        && selectTo.value !== null)
        submitButton.disabled = false;
    else
        submitButton.disabled = true;
};
