import { fib } from "./math";

window.addEventListener('DOMContentLoaded', (e) => {
    let elInput = document.querySelector('input[name=imie]') as HTMLInputElement;
    console.log(elInput.value);
    let elP = document.querySelector('p') as HTMLParagraphElement;
    elP.textContent = 'tekst zmodyfikowany, zmodyfikowany tekst.';
    let nowyDiv = document.createElement('div');
    nowyDiv.innerHTML= '<p><b>NOWY DIV - najnowszy commit w typescripcie:</b></p>';
    let staryDiv = document.querySelector('div[class=container]') as HTMLDivElement;
    staryDiv.appendChild(nowyDiv);

    teczoweKolory(nowyDiv);

    tsGitHubInfoPromise.then( res => {
        //console.log("WYNIKI:\n", res[0]);
        let avatar = document.createElement('img') as HTMLImageElement;
        let author = document.createElement('p') as HTMLParagraphElement;
        let authorDiv = document.createElement('div') as HTMLDivElement;
        avatar.src = res[0].author.avatar_url;
        avatar.width = 200;
        avatar.height = 200;
        author.textContent = res[0].author.login;
        authorDiv.appendChild(author)
        authorDiv.appendChild(avatar);
        nowyDiv.appendChild(authorDiv);
    });

    let grid = document.querySelector('div[class=container]') as HTMLDivElement;
    grid.addEventListener('click', makeRed, false);
    let form = document.querySelector('form[class=reservationForm]') as HTMLFormElement;
    form.addEventListener('change', checkInput);
    let submitButton = document.querySelector('input[type=submit]') as HTMLInputElement;
    submitButton.disabled = true;
    form.addEventListener('submit', (e) => {
        let nameInput = document.querySelector('input[name=imie]') as HTMLInputElement;
        let surnameInput = document.querySelector('input[name=nazwisko]') as HTMLInputElement;
        let selectFrom = document.querySelector('select[name=od]') as HTMLSelectElement;
        let selectTo = document.querySelector('select[name=do]') as HTMLSelectElement;

        window.alert(`
        dane rezerwacji: \n
        Imię: ${nameInput.value} \n
        Nazwisko: ${surnameInput.value}\n
        Od: ${selectFrom.value}\n
        Do: ${selectTo.value}`);
})

}, false);

let numerKlikniecia = 0;
setTimeout(() => {
    console.log('Minęły już 2 sekundy...');
  }, 2000);


const waitFor = (time) => {
    var promise = new Promise( function(resolve, reject){ 
        setTimeout(() => resolve("ok"),
        time
    )});
    return promise;
};

const tecza = [
    'red',
    'orange',
    'yellow',
    'green',
    'blue',
    'indigo',
    'purple'
];

const kolorowaPetla = (el, kolorowaTablica, i, max) => {
    waitFor(1000).then( () => {
        //console.log(kolorowaTablica[i]);
        el.style.backgroundColor = kolorowaTablica[i];
        i = (i + 1) % kolorowaTablica.length;
        if(max > 0)
            max --;
        else if (max === 0)
            return;
        return kolorowaPetla(el, kolorowaTablica, i, max);
    });
}

function teczoweKolory(el: HTMLElement) {
    kolorowaPetla(el, tecza, 0, -1);
}


let tsGitHubInfoPromise = new Promise((resolve, reject) => {
    fetch("https://api.github.com/repos/Microsoft/TypeScript/commits")
    .then(res => res.json())
    .then(data => resolve(data));
})

const makeRed = (e) => {
    let delays = document.querySelector('table[class=delaysTable]') as HTMLTableElement;
    let form = document.querySelector('form[class=reservationForm]') as HTMLFormElement;
    if(!delays.contains(e.target) && !form.contains(e.target))
        return;

    if( e.target.style.backgroundColor === 'palevioletred')
        e.target.style.backgroundColor = 'darkseagreen';
    else
        e.target.style.backgroundColor = 'palevioletred';
    console.log("FIB: ", fib(numerKlikniecia));
    numerKlikniecia ++;
};


const checkInput = (e) => {
    let nameInput = document.querySelector('input[name=imie]') as HTMLInputElement;
    let surnameInput = document.querySelector('input[name=nazwisko]') as HTMLInputElement;
    let selectFrom = document.querySelector('select[name=od]') as HTMLSelectElement;
    let selectTo = document.querySelector('select[name=do]') as HTMLSelectElement;

    let submitButton = document.querySelector('input[type=submit]') as HTMLInputElement;
    if(nameInput.value.trim() !== ''
    && surnameInput.value.trim() !== ''
    && selectFrom.value !== null
    && selectTo.value !== null)
        submitButton.disabled = false;
    else
        submitButton.disabled = true;
}
